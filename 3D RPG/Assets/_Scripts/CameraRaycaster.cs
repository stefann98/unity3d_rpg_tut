﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRaycaster : MonoBehaviour {

    public Layer[] layerPriorities = {
        Layer.Enemy,
        Layer.Walkable
    };

    [SerializeField]float distanceToBackground = 100f;
    Camera viewCamera;

    RaycastHit m_hit;

    public RaycastHit hit {
        get { return m_hit; }
    }

    Layer m_layerHit;
    public Layer layerHit {
        get { return m_layerHit; }
    }

    private void Start() {
        viewCamera = Camera.main;
    }

    private void Update() {
        foreach(Layer layer in layerPriorities) {
            var hit = RaycastForLayer(layer);
            if (hit.HasValue) {
                m_hit = hit.Value;
                m_layerHit = layer;
                return;
            }
        }

        m_hit.distance = distanceToBackground;
        m_layerHit = Layer.RaycastEndStop;
    }

    RaycastHit? RaycastForLayer(Layer layer) {
        int layerMask = 1 << (int)layer;
        Ray ray = viewCamera.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;
        bool hasHit = Physics.Raycast(ray, out hit, distanceToBackground, layerMask);
        if (hasHit) {
            return hit;
        }
        return null;
    }

}
