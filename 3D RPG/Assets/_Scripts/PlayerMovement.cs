﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;

[RequireComponent(typeof(ThirdPersonCharacter))]
public class PlayerMovement : MonoBehaviour {

    [SerializeField] float stopRadius = 0.2f;

    ThirdPersonCharacter m_character;
    CameraRaycaster cameraRaycaster;
    Vector3 currentClickTarget;

	// Use this for initialization
	void Start () {
        cameraRaycaster = Camera.main.GetComponent<CameraRaycaster>();
        m_character = GetComponent<ThirdPersonCharacter>();
        currentClickTarget = this.transform.position;
	}

    private void FixedUpdate() {
        MouseMovement();
    }

    private void MouseMovement() {
        if(Input.GetMouseButton(0)) {
            Debug.Log("Cursor raycast hit: " + cameraRaycaster.layerHit);
            switch(cameraRaycaster.layerHit) {
                case Layer.Walkable:
                currentClickTarget = cameraRaycaster.hit.point;
                break;
                case Layer.Enemy:
                Debug.Log("Cannot move to enemy!!");
                break;
                default:
                Debug.Log("If you are here then the game is broken. GO AWAY!");
                return;
            }
        }
        var playerToClick = currentClickTarget - this.transform.position;
        if(playerToClick.magnitude >= stopRadius) {
            m_character.Move(playerToClick, false, false);
        } else {
            m_character.Move(Vector3.zero, false, false);
        }
    }
}
